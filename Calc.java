import java.util.Stack;
public class Calc
{
   private Stack stack;
   private String expr;
   private String output = "";
    
    public Calc(String e) 
    {
        expr = e;
        stack = new Stack();
    }
    
    public String eval()
    {
        for (int i = 0; i < expr.length(); i++)
        {
            char c = expr.charAt(i);
            switch (c) {
                case '+': 
                case '-':
                oper(c, 1); 
                break; 
                case '*': 
                case '/':
                oper(c, 2); 
                break; 
                case '^':
                oper(c, 3);
                break;
                case '(': 
                stack.push(c);
                break;
                case ')': 
                gotParen(c); 
                break;
                default: 
                output = output + c; 
                break;
            }
        }
        while (!stack.isEmpty()) 
        {
            output = output + stack.pop();
        }
        return output;
    }
    
    public void oper(char operation, int prec)
    {
      while (!stack.isEmpty())
      {
         char sTop = (Character) stack.pop();
         if (sTop == '(') 
         {
            stack.push(sTop);
            break;
         }
         else 
         {
            int prec1;
            if (sTop == '+' || sTop == '-')
            {
                prec1 = 1;
            }
            else
            {
                if (sTop == '*' || sTop == '/')
                {
                    prec1 = 2;
                }
                else
                {
                    if (sTop == '^')
                    {
                        prec1 = 3;
                    }
                    else
                    {
                        prec1 = 4;
                    }
                }
            }
            if (prec1 < prec) 
            { 
               stack.push(sTop);
               break;
            }
		    else
		    {
                output = output + sTop;
            }
         }
      }
      stack.push(operation);
   }
    public void gotParen(char c)
    { 
      while (!stack.isEmpty())
      {
         char paran = (Character) stack.pop();
         if (paran == '(') 
         {
            break; 
         }
         else
         {
            output = output + paran; 
         }
      }
   }
    
}