import acm.gui.*;
import acm.program.*;
import javax.swing.*;
import java.awt.event.*;

public class GUI extends Program
{
    // TextFields that both methods need to access
    private JTextField sequenceInput;
    private JTextField sequenceType;
    private JTextField nextValue;
    
    // Constructor. Calls init() via start(), then sets the size of the window.
    public GUI()
    {
        this.start();
        this.setSize(225, 250);
    }
    
    public void init()
    {
        JLabel seqInputLabel = new JLabel("Infix Equation");
        JLabel nextValLabel = new JLabel("Postfix Equation");
        
        JButton goButton = new JButton("Go!");
        goButton.setActionCommand("go");
        
        JButton clearButton = new JButton("Clear");
        clearButton.setActionCommand("clear");
        
        this.sequenceInput = new JTextField();
        this.nextValue = new JTextField();
        
        TableLayout t = new TableLayout(3, 2);
        this.setLayout(t);
        
        this.add(seqInputLabel);
        this.add(sequenceInput);
        this.add(clearButton);
        this.add(goButton);
        this.add(nextValLabel);
        this.add(nextValue);
        
        addActionListeners();
    }
    
    public void actionPerformed(ActionEvent e)
    {
        String cmd = e.getActionCommand();
        if (cmd.equals("go"))
        {
            String seq = sequenceInput.getText();
            Calc p = new Calc(seq);
            nextValue.setText(p.eval());
        }
        if(cmd.equals("clear"))
        {
            nextValue.setText("");
            sequenceInput.setText("");
        }
    }
}